package tools;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

public class PageWeb {
	
	public Vector<String> extensions;
	public PageWeb(){
		extensions = new Vector<String>();
		extensions.add("fr");
		extensions.add("com");
		extensions.add("org");
		extensions.add("net");
		extensions.add("info");
		extensions.add("biz");
		extensions.add("de");
		extensions.add("be");
		extensions.add("it");
		extensions.add("uk");
		extensions.add("us");
		extensions.add("es");
		extensions.add("ch");
	}

	
	/**
	 * 
	 * @param s, the text where to search
	 * @param tag, the tag to extract
	 * @return the content in the tag
	 */
	public String getContentTag(String s, String tag){
		String content = "";
		
		System.out.println(s);
		
		//removes all the whitespace and non-visible characters.
		s = s.replaceAll("\\s+","");
				
		System.out.println(s);
		
		//find the index of the begining of the tag
		int index_begining = s.indexOf("<"+tag);
		
		//find the index of the end of the tag.
		int index_end = s.indexOf(">",index_begining);
		
		//find the index of the begining of the second tag.
		int index_end_content = s.indexOf("</"+tag+">");
		
		content = s.substring(index_end+1, index_end_content);

		return content;
	}
	
	/**
	 * Get all the content of all the tags 
	 * @param s
	 * @param tag
	 * @param withTag
	 * @return
	 */
	public List<String> getContentAllTags(String s, String tag, boolean withTag){
		List<String> list = new ArrayList<String>();
		
		//removes all the whitespace and non-visible characters.
		s = s.replaceAll("\\s+","");
		
		//find the index of the begining of the tag
		
		boolean search = true;
		String content;
		int tmp = 0;
		
		while (search){
			content = "";
			int index_begining = s.indexOf("<"+tag, tmp);
			
			if (index_begining!= -1){
				
				int index_end = s.indexOf(">",index_begining);
				
				if (index_end != -1){
				
				
					int index_end_content = s.indexOf("</"+tag+">", index_end);
					
					if (index_end_content != -1){
						
						
						if (withTag){
							content = s.substring(index_begining, index_end_content+3+tag.length());
						}else{
							content = s.substring(index_end+1, index_end_content);
						}	
						tmp = index_end_content + 3 + tag.length();
						list.add(content);
					}
				}
			}
			if (content == "")
				search = false;
				
		}
		return list;
	}
	
	/**
	 * get the value of attribute att in a content s
	 * @param s
	 * @param att
	 * @return
	 */
	public String getAttribute(String s, String att){
		
		//get the begining of the attribute (href, class, ...)
		int index = s.indexOf(att);
		//System.out.println("index = "+index);
		
		// +1 because of the =" " 
		int index_end = s.indexOf("\"",index+att.length()+2);
		//System.out.println("indexend = "+index_end);
		
		
		String content= null;
		if (index != -1 && index_end != -1){
			content= s.substring(index+att.length()+2,index_end );
		}
		return content;
	}
	
	
	public LinkedList<String> getAllURL(String s){
		LinkedList<String> list = new LinkedList<String>();
		
		List<String> tags_a = this.getContentAllTags(s, "a",true);
		
		for (String c : tags_a){
			String ss =this.getAttribute(c, "href");
			System.out.println("ss = "+ss);
			String name = this.getName(ss);
			if (name != null)
				list.add(name);
		}
		return list;
	}
	
	/// get the content of the page at the URL s
	public String getContent(String s) throws MalformedURLException, IOException{
		HttpURLConnection conn = (HttpURLConnection) new URL(s).openConnection();
	    conn.connect();
 
        BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
 
        byte[] bytes = new byte[1024];
        int tmp ;
        String content = "";
		while( (tmp = bis.read(bytes) ) != -1 ) {
		    String chaine = new String(bytes,0,tmp);
		    content  += chaine;
		}
		conn.disconnect();
		return content;
	}
	
	public boolean isSame(String s){
		return s.charAt(0)=='/' || s.substring(0, 2).equals("..");
	}

	//get the name of the website according to the url s
	public String getName(String s){
		if (s==null)
			return null;
		
		int i = 0;
		int index=-1;
		String name = null;
		//test if there is an extension for this url
		while (i<this.extensions.size() && (index = s.indexOf("."+this.extensions.get(i))) == -1)
			i++;
		
		
		//if there is an extension
		if (index != -1){
			String without_extension = s.substring(0, index);
			int index2 = without_extension.lastIndexOf('.');
			//System.out.println(s.substring(index2+1, index));
			
			if (index2 != -1){
				name = "http://www."+s.substring(index2+1, index)+"."+this.extensions.get(i);
			}
			else{
				//index2 = without_extension.lastIndexOf('/');
				//name = "http://www."+s.substring(index2+1, index)+"."+this.extensions.get(i);
				name = null;
			}
			//int index2 = without_extension.lastIndexOf('/');
			
		}
		return name;
	}

	
}
