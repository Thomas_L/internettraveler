package main;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import tools.PageWeb;

public class Main {
	public static void main(String [] args){
		browseTheInternet("https://fr.wikipedia.org/wiki/C%C3%A9sar_Borgia");
		/*
		PageWeb p = new PageWeb();
		String lol = null;
		try {
			lol = p.getContent("https://www.wikipedia.org");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("content = |"+lol+"|");*/
	}
	
	public static void browseTheInternet(String start){
		
		PageWeb p = new PageWeb();
		
		String content=null;
		Map<String,List<String>> map = new HashMap<String,List<String>>();

		//System.out.println(content);
		LinkedList<String> urls = new LinkedList<String>();//p.getAllURL(content);
		LinkedList<String> tmp;
		urls.add(start);
		
		int to = 0;
		while (!urls.isEmpty() && to <100){
			String url = urls.removeFirst();
			
			if (map.containsKey(url) == false)
				map.put(url, new ArrayList<String>());
			
			System.out.println("Current site : "+url);
			

			try {
				content = p.getContent(url);
			} catch (IOException e) {
				//e.printStackTrace();
			}
			
			// get all the urls in the web page
			tmp = p.getAllURL(content);
			
			// add them to the fifo
			for (String n : tmp){
				System.out.println("\t- "+n);
				map.get(url).add(n);
				if (map.containsKey(n)==false)
					urls.addLast(n);
				
			}
			to++;
		}
		
	}
	
}
