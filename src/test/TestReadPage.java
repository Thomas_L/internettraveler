package test;

import java.io.BufferedInputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class TestReadPage {
	public static void main(String[] args) throws Exception {
        HttpURLConnection conn = (HttpURLConnection) new URL(
                "http://www.free.fr/").openConnection();
        conn.connect();
 
        BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
 
        byte[] bytes = new byte[1024];
        int tmp ;
        while( (tmp = bis.read(bytes) ) != -1 ) {
            String chaine = new String(bytes,0,tmp);
            System.out.print(chaine);
            //System.out.println("===========");
        }
         
        conn.disconnect();
    }
}
