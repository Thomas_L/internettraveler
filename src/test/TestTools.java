package test;

import java.util.List;

import tools.PageWeb;

public class TestTools {
	
	public static void main(String [] args){
		PageWeb page = new PageWeb();
		
		
		String content = page.getContentTag("< ceci > est un tst </ceci>","ceci");
		System.out.println(content);
		
		System.out.println("==========================");
		
		
		String s = 
				 "<a meta=\"lool\" href=\"loo\" >lol </a>"
				+ "<a href=\"lool\" meta = \"loool\">slkg</a>ttt"
				+ "<a>lool</a></a>";
		
		System.out.println(s);
		List<String> contents = page.getContentAllTags(s, "a",true);
		
		for (String c : contents){
			System.out.println(c);
			System.out.println("href="+page.getAttribute(c, "href"));
			System.out.println("============");
		}
	}
}
